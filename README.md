# EZ65C02 Read Me

## Description

**EaZy 65C02** is a Western Design Center 65C02 emulator witten in C++ and designed for simplicity, portability and extensivity and it's the emulation core of the ZerOS project.
It aims to be an opcode interpreter (not cycle accurate) for MCUs that due to their Harvard architecture cannot load programs at runtime.
In can also be used in any situation where a virtual machine that understands 65C02 opcodes is needed.
WDM opcode is used to provide additional high level routines.
Functions inside the file **EZ65C02Ext.cpp** can be used to extend the instruction set or to implement memory mapping features at any address above 0x01FF (which contains the stack and the zero page memory).

Further informations about the 6502 CPU (from which the 65c02 derives) can be fount at http://www.6502.org/

## Canonical source

The source of the 65C02 Project is [hosted on GitLab.com](https://gitlab.com/masteraldo/ez65c02).

## Usage
The projects has been developed in C++ using Visual Studio 2019 but it can be easily compiled in any other environment.
The only external library needed to compile it is **cstdint** plus **stdio.h** and **conio.h** if it is needed to compile the test program.
**windows.h** is used to resize the console of the test program, but is not mandatory.

## License
(c) 2021 Eraldo Zanon - EZ65C02 Project
This code is licensed under MIT license (see LICENSE.txt for details) 