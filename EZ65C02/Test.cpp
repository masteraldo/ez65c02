/*
   _____ _______  ____   ____ ___ ____
  | ____|__  / /_| ___| / ___/ _ \___ \
  |  _|   / / '_ \___ \| |  | | | |__) |
  | |___ / /| (_) |__) | |__| |_| / __/
  |_____/____\___/____/ \____\___/_____| P R O J E C T

A WDC65C02 emulator designed for simplicity, portability and extensivity.
It aims to be an opcode interpreter (not cycle accurate) for MCUs and it's the emulator core of the ZerOS project.
WDM opcode is used to provide additional high level routines.

(c) 2021 Eraldo Zanon - EZ65C02 Project
This code is licensed under MIT license (see LICENSE.txt for details)
*/

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
#include <windows.h>
#endif
#include <stdio.h>
#include <cstdint>
#include <conio.h>
#include "EZ65C02.h"

//Bytes to opcode mapping for logging purposes
constexpr char OPCODE_NAMES[256][32]    = { "BRK_imp                  ", "ORA_mem(IndirectX)       ", "NOP_imp(1)               ", "NOP_imp(0)               ", "TSB_mem(ZeroPage)        ", "ORA_mem(ZeroPage)        ", "ASL_mem(ZeroPage)        ", "RMB_mem(ZeroPage,0xFE)   ", "PHP_imp                  ", "ORA_mem(Immediate)       ", "ASL_acc                  ", "NOP_imp(0)               ", "TSB_mem(Absolute)        ", "ORA_mem(Absolute)        ", "ASL_mem(Absolute)        ", "BBR_mem(ZeroPage,0x01)   ", "BPL_mem                  ", "ORA_mem(IndirectY)       ", "ORA_mem(IndirectZeroPage)", "NOP_imp(0)               ", "TRB_mem(ZeroPage)        ", "ORA_mem(ZeroPageX)       ", "ASL_mem(ZeroPageX)       ", "RMB_mem(ZeroPage,0xFD)   ", "CLC_imp                  ", "ORA_mem(AbsoluteY)       ", "INC_acc                  ", "NOP_imp(0)               ", "TRB_mem(Absolute)        ", "ORA_mem(AbsoluteX)       ", "ASL_mem(AbsoluteX)       ", "BBR_mem(ZeroPage,0x02)   ", "JSR_mem(Absolute)        ", "AND_mem(IndirectX)       ", "NOP_imp(1)               ", "NOP_imp(0)               ", "BIT_mem(ZeroPage)        ", "AND_mem(ZeroPage)        ", "ROL_mem(ZeroPage)        ", "RMB_mem(ZeroPage,0xFB)   ", "PLP_imp                  ", "AND_mem(Immediate)       ", "ROL_acc                  ", "NOP_imp(0)               ", "BIT_mem(Absolute)        ", "AND_mem(Absolute)        ", "ROL_mem(Absolute)        ", "BBR_mem(ZeroPage,0x04)   ", "BMI_mem                  ", "AND_mem(IndirectY)       ", "AND_mem(IndirectZeroPage)", "NOP_imp(0)               ", "BIT_mem(ZeroPageX)       ", "AND_mem(ZeroPageX)       ", "ROL_mem(ZeroPageX)       ", "RMB_mem(ZeroPage,0xF7)   ", "SEC_imp                  ", "AND_mem(AbsoluteY)       ", "DEC_acc                  ", "NOP_imp(0)               ", "BIT_mem(AbsoluteX)       ", "AND_mem(AbsoluteX)       ", "ROL_mem(AbsoluteX)       ", "BBR_mem(ZeroPage,0x08)   ", "RTI_imp                  ", "EOR_mem(IndirectX)       ", "NOP_imp(1)               ", "NOP_imp(0)               ", "NOP_imp(1)               ", "EOR_mem(ZeroPage)        ", "LSR_mem(ZeroPage)        ", "RMB_mem(ZeroPage,0xEF)   ", "PHA_imp                  ", "EOR_mem(Immediate)       ", "LSR_acc                  ", "NOP_imp(0)               ", "JMP_mem(Absolute)        ", "EOR_mem(Absolute)        ", "LSR_mem(Absolute)        ", "BBR_mem(ZeroPage,0x10)   ", "BVC_mem                  ", "EOR_mem(IndirectY)       ", "EOR_mem(IndirectZeroPage)", "NOP_imp(0)               ", "NOP_imp(1)               ", "EOR_mem(ZeroPageX)       ", "LSR_mem(ZeroPageX)       ", "RMB_mem(ZeroPage,0xDF)   ", "CLI_imp                  ", "EOR_mem(AbsoluteY)       ", "PHY_imp                  ", "NOP_imp(0)               ", "NOP_imp(2)               ", "EOR_mem(AbsoluteX)       ", "LSR_mem(AbsoluteX)       ", "BBR_mem(ZeroPage,0x20)   ", "RTS_imp                  ", "ADC_mem(IndirectX)       ", "NOP_imp(1)               ", "NOP_imp(0)               ", "STZ_mem(ZeroPage)        ", "ADC_mem(ZeroPage)        ", "ROR_mem(ZeroPage)        ", "RMB_mem(ZeroPage,0xBF)   ", "PLA_imp                  ", "ADC_mem(Immediate)       ", "ROR_acc                  ", "NOP_imp(0)               ", "JMP_mem(Indirect)        ", "ADC_mem(Absolute)        ", "ROR_mem(Absolute)        ", "BBR_mem(ZeroPage,0x40)   ", "BVS_mem                  ", "ADC_mem(IndirectY)       ", "ADC_mem(IndirectZeroPage)", "NOP_imp(0)               ", "STZ_mem(ZeroPageX)       ", "ADC_mem(ZeroPageX)       ", "ROR_mem(ZeroPageX)       ", "RMB_mem(ZeroPage,0x7F)   ", "SEI_imp                  ", "ADC_mem(AbsoluteY)       ", "PLY_imp                  ", "NOP_imp(0)               ", "JMP_mem(IndirectAbsX)    ", "ADC_mem(AbsoluteX)       ", "ROR_mem(AbsoluteX)       ", "BBR_mem(ZeroPage,0x80)   ", "BRA_mem                  ", "STA_mem(IndirectX)       ", "NOP_imp(1)               ", "NOP_imp(0)               ", "STY_mem(ZeroPage)        ", "STA_mem(ZeroPage)        ", "STX_mem(ZeroPage)        ", "SMB_mem(ZeroPage,0x01)   ", "DEY_imp                  ", "BIT_mem(Immediate)       ", "TXA_imp                  ", "NOP_imp(0)               ", "STY_mem(Absolute)        ", "STA_mem(Absolute)        ", "STX_mem(Absolute)        ", "BBS_mem(ZeroPage,0x01)   ", "BCC_mem                  ", "STA_mem(IndirectY)       ", "STA_mem(IndirectZeroPage)", "NOP_imp(0)               ", "STY_mem(ZeroPageX)       ", "STA_mem(ZeroPageX)       ", "STX_mem(ZeroPageY)       ", "SMB_mem(ZeroPage,0x02)   ", "TYA_imp                  ", "STA_mem(AbsoluteY)       ", "TXS_imp                  ", "NOP_imp(0)               ", "STZ_mem(Absolute)        ", "STA_mem(AbsoluteX)       ", "STZ_mem(AbsoluteX)       ", "BBS_mem(ZeroPage,0x02)   ", "LDY_mem(Immediate)       ", "LDA_mem(IndirectX)       ", "LDX_mem(Immediate)       ", "NOP_imp(0)               ", "LDY_mem(ZeroPage)        ", "LDA_mem(ZeroPage)        ", "LDX_mem(ZeroPage)        ", "SMB_mem(ZeroPage,0x04)   ", "TAY_imp                  ", "LDA_mem(Immediate)       ", "TAX_imp                  ", "NOP_imp(0)               ", "LDY_mem(Absolute)        ", "LDA_mem(Absolute)        ", "LDX_mem(Absolute)        ", "BBS_mem(ZeroPage,0x04)   ", "BCS_mem                  ", "LDA_mem(IndirectY)       ", "LDA_mem(IndirectZeroPage)", "NOP_imp(0)               ", "LDY_mem(ZeroPageX)       ", "LDA_mem(ZeroPageX)       ", "LDX_mem(ZeroPageY)       ", "SMB_mem(ZeroPage,0x08)   ", "CLV_imp                  ", "LDA_mem(AbsoluteY)       ", "TSX_imp                  ", "NOP_imp(0)               ", "LDY_mem(AbsoluteX)       ", "LDA_mem(AbsoluteX)       ", "LDX_mem(AbsoluteY)       ", "BBS_mem(ZeroPage,0x08)   ", "CPY_mem(Immediate)       ", "CMP_mem(IndirectX)       ", "NOP_imp(1)               ", "NOP_imp(0)               ", "CPY_mem(ZeroPage)        ", "CMP_mem(ZeroPage)        ", "DEC_mem(ZeroPage)        ", "SMB_mem(ZeroPage,0x10)   ", "INY_imp                  ", "CMP_mem(Immediate)       ", "DEX_imp                  ", "WAI_imp                  ", "CPY_mem(Absolute)        ", "CMP_mem(Absolute)        ", "DEC_mem(Absolute)        ", "BBS_mem(ZeroPage,0x10)   ", "BNE_mem                  ", "CMP_mem(IndirectY)       ", "CMP_mem(IndirectZeroPage)", "NOP_imp(0)               ", "NOP_imp(1)               ", "CMP_mem(ZeroPageX)       ", "DEC_mem(ZeroPageX)       ", "SMB_mem(ZeroPage,0x20)   ", "CLD_imp                  ", "CMP_mem(AbsoluteY)       ", "PHX_imp                  ", "STP_imp                  ", "NOP_imp(2)               ", "CMP_mem(AbsoluteX)       ", "DEC_mem(AbsoluteX)       ", "BBS_mem(ZeroPage,0x20)   ", "CPX_mem(Immediate)       ", "SBC_mem(IndirectX)       ", "NOP_imp(1)               ", "NOP_imp(0)               ", "CPX_mem(ZeroPage)        ", "SBC_mem(ZeroPage)        ", "INC_mem(ZeroPage)        ", "SMB_mem(ZeroPage,0x40)   ", "INX_imp                  ", "SBC_mem(Immediate)       ", "NOP_imp(0)               ", "NOP_imp(0)               ", "CPX_mem(Absolute)        ", "SBC_mem(Absolute)        ", "INC_mem(Absolute)        ", "BBS_mem(ZeroPage,0x40)   ", "BEQ_mem                  ", "SBC_mem(IndirectY)       ", "SBC_mem(IndirectZeroPage)", "NOP_imp(0)               ", "NOP_imp(1)               ", "SBC_mem(ZeroPageX)       ", "INC_mem(ZeroPageX)       ", "SMB_mem(ZeroPage,0x80)   ", "SED_imp                  ", "SBC_mem(AbsoluteY)       ", "PLX_imp                  ", "NOP_imp(0)               ", "NOP_imp(2)               ", "SBC_mem(AbsoluteX)       ", "INC_mem(AbsoluteX)       ", "BBS_mem(ZeroPage,0x80)   " };
constexpr char const* PROGRAM_FILE_NAME = ".\\Test Files\\6502_functional_test.bin"; //Program file name to run
constexpr uint16_t LOAD_START_ADDRESS   = 0;     //Specifies at what address in memory the file should start to be loaded. For memory image files (65535 bytes) this value must be set to 0               
constexpr uint64_t SKIPPED_DEBUG_CYCLES = 1000;  //How many cycles the vpu must execute before a log is printed out. 1 means every cycle
constexpr bool DEBUG_MODE               = true;  //Prints debug logs and halts emulation on invalid opcodes
constexpr bool STEP_BY_STEP             = false; //Executes the cpu step by step, one instruction at time

//Evil hack to read private fields without having to make friends class or modify the EZ65C02 source
typedef struct
{
    uint8_t internalMemory[INT_MEM_TOP_ADDR + 1];
    uint16_t startAddr; //The starting address the program counter is set on startup and reset
    uint16_t regPC;     //16 bit program counter
    uint8_t regSP;      //8 bit stack pointer
    uint8_t regA;       //8 bit accumulator
    uint8_t regX;       //8 bit X register
    uint8_t regY;       //8 bit Y register
    uint8_t flagN;      //Status register Sign flag
    uint8_t flagV;      //Status register Overflow flag
    uint8_t flagD;      //Status register Decimal Mode flag
    uint8_t flagI;      //Status register Disable Interrupts flag
    uint8_t flagZ;      //Status register Zero flag
    uint8_t flagC;      //Status register Carry flag
    //flagB doesn't exist inside the 65C02 status register, it's set only when pushed on the stack by BRK or PHP

    volatile uint8_t  interruptStatus; //Set of 3 bits to handle external interrupts: 0 = No interrupts, 00000001 = RESET, 00000010 = NMI, 00000100 = IRQ
} PrivateReaderHack;


//Write here the condition that makes the emulation halt when a test success condition is met. 
bool checkForTestSuccess(PrivateReaderHack* VPUInspector)
{
    return VPUInspector->regPC == 0x3469; //Klaus Dormann's 6502 Test suite success address. If the emulator reaches this address it works correctly
}

//Program loader
errno_t  loadFile(const char* fileName)
{
    FILE* hex;
    int data    = -1;
    errno_t err = fopen_s(&hex, fileName, "rb");

    if (err || hex == 0)
    {
        return -1;
    }

    //Change 0 to another value if the program must be loaded from another starting address
    for (uint32_t addr = LOAD_START_ADDRESS; (data = getc(hex)) != EOF; ++addr)
    {
        if (addr == MAX_MEMORY_SIZE)
        {
            return -2;
        }

        VPU.writeMem((uint16_t)addr, (uint8_t)data);
    }

    return 0;
}

//Quick & Dirty log function to print registers, memory and opcodes.
//Step by step execution is provided if flag STEP_BY_STEP is set.
//An exlamation mark near a cpu flag/register means that the value has been changed since the last cycle
void printCPUStatus(PrivateReaderHack* VPUInspector)
{
    static uint64_t cycles = 0;
    unsigned char c        = 0; 
    uint8_t opcode         = VPU.readMem(VPUInspector->regPC);
    static uint8_t oldA;
    static uint8_t oldX;
    static uint8_t oldY;
    static uint8_t oldSP;
    static uint8_t oldN;
    static uint8_t oldV;
    static uint8_t oldD;
    static uint8_t oldI;
    static uint8_t oldZ;
    static uint8_t oldC;
    static uint8_t oldInterruptStatus;

    if (cycles % SKIPPED_DEBUG_CYCLES != 0L)
    {
        ++cycles;
        return;
    }

    printf("%012llu Address: %04X - Opcode: %02X %s", cycles, VPUInspector->regPC, opcode, OPCODE_NAMES[opcode]);

    if (VPUInspector->regA != oldA)
    {
        printf(" - A: %02X!", VPUInspector->regA);
    }
    else
    {
        printf(" - A: %02X ", VPUInspector->regA);
    }
    if (VPUInspector->regX != oldX)
    {
        printf(" - X: %02X!", VPUInspector->regX);
    }
    else
    {
        printf(" - X: %02X ", VPUInspector->regX);
    }
    if (VPUInspector->regY != oldY)
    {
        printf(" - Y: %02X!", VPUInspector->regY);
    }
    else
    {
        printf(" - Y: %02X ", VPUInspector->regY);
    }
    if (VPUInspector->regSP != oldSP)
    {
        printf(" - SP: %02X!", VPUInspector->regSP);
    }
    else
    {
        printf(" - SP: %02X ", VPUInspector->regSP);
    }

    printf(" - NVssDIZC: ");

    if (VPUInspector->flagN != oldN)
    {
        printf("%u!", VPUInspector->flagN);
    }
    else
    {
        printf("%u ", VPUInspector->flagN);
    }

    if (VPUInspector->flagV != oldV)
    {
        printf("%u!", VPUInspector->flagV);
    }
    else
    {
        printf("%u ", VPUInspector->flagV);
    }

    printf("1 0 ");

    if (VPUInspector->flagD != oldD)
    {
        printf("%u!", VPUInspector->flagD);
    }
    else
    {
        printf("%u ", VPUInspector->flagD);
    }

    if (VPUInspector->flagI != oldI)
    {
        printf("%u!", VPUInspector->flagI);
    }
    else
    {
        printf("%u ", VPUInspector->flagI);
    }

    if (VPUInspector->flagZ != oldZ)
    {
        printf("%u!", VPUInspector->flagZ);
    }
    else
    {
        printf("%u ", VPUInspector->flagZ);
    }

    if (VPUInspector->flagC != oldC)
    {
        printf("%u!  ", VPUInspector->flagC);
    }
    else
    {
        printf("%u   ", VPUInspector->flagC);
    }

    if (VPUInspector->interruptStatus != oldInterruptStatus)
    {
        printf("Interrupt Status: %u!\n", VPUInspector->interruptStatus);
    }
    else
    {
        printf("Interrupt Status: %u \n", VPUInspector->interruptStatus);
    }

    oldSP = VPUInspector->regSP;
    oldA  = VPUInspector->regA;
    oldX  = VPUInspector->regX;
    oldY  = VPUInspector->regY;
    oldN  = VPUInspector->flagN;
    oldV  = VPUInspector->flagV;
    oldD  = VPUInspector->flagD;
    oldI  = VPUInspector->flagI;
    oldZ  = VPUInspector->flagZ;
    oldC  = VPUInspector->flagC;
    oldInterruptStatus = VPUInspector->interruptStatus;

    if (checkForTestSuccess(VPUInspector))
    {
        printf("\r\n           S U C C E S S !   T E S T   P A S S E D .\r\n");
        printf("               EXECUTION HALTED.\r\n");
        exit(0);
    }

    ++cycles;

    if (STEP_BY_STEP) 
    {
        c = _getch();
    }

    if (c >= '0' && c <= '7')   //Each choice from 0 to 7 Prints a window of 8192 bytes for a total of 64KiB, the whole amount of addressable memory
    {
        printf("\n");

        for (int i = (c - '0') * 8192, len = i + 8192; i < len; i += 8)
        {
            printf("0x%04X:     %02X  %02X  %02X  %02X  %02X  %02X  %02X  %02X\n", i, VPU.readMem(i), VPU.readMem(i + 1), VPU.readMem(i + 2), VPU.readMem(i + 3), VPU.readMem(i + 4), VPU.readMem(i + 5), VPU.readMem(i + 6), VPU.readMem(i + 7));
        }

        printf("\n");
    }
    else if (c == 'z')  //Prints the zero page memory
    {
        printf("\n");

        for (int i = 0x00, len = 0x100; i < len; i += 8)
        {
            printf("0x%04X:     %02X  %02X  %02X  %02X  %02X  %02X  %02X  %02X\n", i, VPU.readMem(i), VPU.readMem(i + 1), VPU.readMem(i + 2), VPU.readMem(i + 3), VPU.readMem(i + 4), VPU.readMem(i + 5), VPU.readMem(i + 6), VPU.readMem(i + 7));
        }

        printf("\n");

    }
    else if (c == 's')  //Prints the stack
    {
        printf("\n");

        for (int i = 0x100, len = 0x200; i < len; i += 8)
        {
            printf("0x%04X:     %02X  %02X  %02X  %02X  %02X  %02X  %02X  %02X\n", i, VPU.readMem(i), VPU.readMem(i + 1), VPU.readMem(i + 2), VPU.readMem(i + 3), VPU.readMem(i + 4), VPU.readMem(i + 5), VPU.readMem(i + 6), VPU.readMem(i + 7));
        }

        printf("\n");
    }
}

//Application entry point
int main()
{
    //If on Windows The console is automatically set to a more accomodating size for logs
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
    HWND console = GetConsoleWindow();
    RECT r;
    GetWindowRect(console, &r); //stores the console's current dimensions
    MoveWindow(console, r.left, r.top, 1280, 640, TRUE); // Sets new dimensions
    #endif

    //Super evil hack to map the EZ65C02 object in a struct with the same memory layout and expose VPU private fields
    PrivateReaderHack* VPUInspector = reinterpret_cast<PrivateReaderHack*>(&VPU);
    errno_t  fileLoadStatus;

    //Loads a program starting from memory address 0
    fileLoadStatus = loadFile(PROGRAM_FILE_NAME);

    if (fileLoadStatus == 0)
    {
        //Initializes registers and sets starting address
        VPU.init(DEFAULT_PC_START_ADDR);

        //Emulate!
        for (;;)
        {
            if (DEBUG_MODE)
            {
                printCPUStatus(VPUInspector);
            }

            VPU.execCycle();
        }

        return 0;
    }
    else if (fileLoadStatus == -1)
    {
        printf("Specified file does not exist!\n");
        return fileLoadStatus;
    }
    else if (fileLoadStatus == -2)
    {
        printf("Specified file size exceedes the maximum size of 65536 bytes!\n");
        return fileLoadStatus;
    }
    else
    {
        printf("Unknown error!\n");
        return fileLoadStatus;
    }
}