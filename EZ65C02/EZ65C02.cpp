/*
   _____ _______  ____   ____ ___ ____  
  | ____|__  / /_| ___| / ___/ _ \___ \ 
  |  _|   / / '_ \___ \| |  | | | |__) |
  | |___ / /| (_) |__) | |__| |_| / __/ 
  |_____/____\___/____/ \____\___/_____| P R O J E C T
  
A WDC65C02 emulator designed for simplicity, portability and extensivity.
It aims to be an opcode interpreter (not cycle accurate) for MCUs and it's the emulator core of the ZerOS project. 
WDM opcode is used to provide additional high level routines.

(c) 2021 Eraldo Zanon - EZ65C02 Project
This code is licensed under MIT license (see LICENSE.txt for details)
*/

#include <cstdint>
#include "EZ65C02.h"

//Virtual Processing Unit instance definition
EZ65C02 VPU;

//Address calculation functions
uint16_t EZ65C02::getAddrImmediate()
{
    return this->regPC++;
}

uint16_t EZ65C02::getAddrRelative()
{
    int8_t relAddr = this->readMem(this->regPC++);

    return relAddr + this->regPC;
}

uint8_t EZ65C02::getAddrZeroPage()
{
    return this->readMem(this->regPC++);
}

uint8_t EZ65C02::getAddrZeroPageX()
{
    return this->readMem(this->regPC++) + this->regX;
}

uint8_t EZ65C02::getAddrZeroPageY()
{
    return this->readMem(this->regPC++) + this->regY;
}

uint16_t EZ65C02::getAddrAbsolute()
{
    uint16_t ea = this->readMem16(this->regPC);

    this->regPC += 2;

    return ea;
}

uint16_t EZ65C02::getAddrAbsoluteX()
{
    uint16_t ea = this->readMem16(this->regPC) + this->regX;

    this->regPC += 2;

    return ea;
}

uint16_t EZ65C02::getAddrAbsoluteY()
{
    uint16_t ea = this->readMem16(this->regPC) + this->regY;

    this->regPC += 2;

    return ea;
}

uint16_t EZ65C02::getAddrIndirect()
{
    uint16_t ea = this->readMem16(this->regPC);

    this->regPC += 2;

    return this->readMem16(ea);
}

uint16_t EZ65C02::getAddrIndirectZeroPage()
{
    uint8_t ea = this->readMem(this->regPC++);

    return this->readMemZeroPage16(ea);
}

uint16_t EZ65C02::getAddrIndirectX()
{
    uint8_t ea = this->readMem(this->regPC++) + this->regX;

    return this->readMemZeroPage16(ea);
}

uint16_t EZ65C02::getAddrIndirectY()
{
    uint8_t ea = this->readMem(this->regPC++);

    return this->readMemZeroPage16(ea) + this->regY;
}

uint16_t EZ65C02::getAddrIndirectAbsX()
{
    uint16_t ea = this->readMem16(this->regPC) + this->regX;

    this->regPC += 2;

    return this->readMem16(ea);
}


//Memory read/write functions
uint8_t EZ65C02::readMem(uint16_t address)
{
    if (address <= INT_MEM_TOP_ADDR)
    {
        return this->internalMemory[address];
    }
    else
    {
        return this->readExtMem(address - INT_MEM_TOP_ADDR - 1);
    }
}

void EZ65C02::writeMem(uint16_t address, uint8_t data)
{
    if (address <= INT_MEM_TOP_ADDR)
    {
        this->internalMemory[address] = data;
    }
    else
    {
        this->writeExtMem(address - INT_MEM_TOP_ADDR - 1, data);
    }
}

uint16_t EZ65C02::readMem16(uint16_t address)
{
    uint16_t value;

    if (address < INT_MEM_TOP_ADDR)
    {
        value = this->internalMemory[address];
        value |= ((uint16_t)this->internalMemory[address + 1]) << 8;

        return value;
    }
    else if (address > INT_MEM_TOP_ADDR)
    {
        return this->readExtMem16(address - INT_MEM_TOP_ADDR - 1);
    }
    else
    {
        value = this->internalMemory[address];
        value |= ((uint16_t)this->readExtMem(address - INT_MEM_TOP_ADDR)) << 8;

        return value;
    }
}

void EZ65C02::writeMem16(uint16_t address, uint16_t data)
{
    if (address < INT_MEM_TOP_ADDR)
    {
        this->internalMemory[address] = (uint8_t)data;
        this->internalMemory[address + 1] = data >> 8;
    }
    else if (address > INT_MEM_TOP_ADDR)
    {
        this->writeExtMem16(address - INT_MEM_TOP_ADDR - 1, data);
    }
    else
    {
        this->internalMemory[address] = (uint8_t)data;
        this->writeExtMem(address - INT_MEM_TOP_ADDR, data >> 8);
    }
}

uint8_t EZ65C02::readMemZeroPage(uint8_t address)
{
    return this->internalMemory[address];
}

void EZ65C02::writeMemZeroPage(uint8_t address, uint8_t data)
{
    this->internalMemory[address] = data;
}

uint16_t EZ65C02::readMemZeroPage16(uint8_t address)
{
    uint16_t value;

    value = this->internalMemory[address];
    value |= ((uint16_t)this->internalMemory[(uint8_t)(address + 1)]) << 8;

    return value;
}

void EZ65C02::writeMemZeroPage16(uint8_t address, uint16_t data)
{
    this->internalMemory[address] = (uint8_t)data;
    this->internalMemory[(uint8_t)(address + 1)] = data >> 8;
}

uint8_t EZ65C02::pullStack()
{
    ++this->regSP;

    return this->internalMemory[STACK_BOTTOM_ADDR + this->regSP];
}

void EZ65C02::pushStack(uint8_t data)
{
    this->internalMemory[STACK_BOTTOM_ADDR + this->regSP] = data;

    --this->regSP;
}

uint16_t EZ65C02::pullStack16()
{
    this->regSP += 2;

    return this->internalMemory[STACK_BOTTOM_ADDR + (uint8_t)(this->regSP - 1)] | (this->internalMemory[STACK_BOTTOM_ADDR + this->regSP] << 8);
}

void EZ65C02::pushStack16(uint16_t data)
{
    this->internalMemory[STACK_BOTTOM_ADDR + this->regSP] = data >> 8;
    this->internalMemory[STACK_BOTTOM_ADDR + (uint8_t)(this->regSP - 1)] = (uint8_t)data;

    this->regSP -= 2;
}


//Status register manipulation functions
void EZ65C02::testZeroFlag(uint8_t n)                                { this->flagZ = n == 0;    }                               //Sets Zero flag if n is 0
void EZ65C02::testNegativeFlag(int8_t  n)                            { this->flagN = n < 0;     }                               //Sets Negative flag if signed representation of n is negative
void EZ65C02::testCarryFlag(uint16_t n)                              { this->flagC = n > 0xFF;  }                               //Sets the carry flag if n is bigger than 255
void EZ65C02::testCarryFlagSub(uint16_t n)                           { this->flagC = n < 0x100; }                               //Sets the carry flag after SBC if the minuend is less than the subtrahend and a wraparound occurs
void EZ65C02::testCarryFlagShiftRight(uint8_t n)                     { this->flagC = n & 1;     }                               //Sets the carry flag if the rightmost bit before shift is 1
void EZ65C02::testCarryFlagShiftLeft(uint8_t n)                      { this->flagC = n > 0x7F;  }                               //Sets the carry flag if the leftmost bit before shift is 1
void EZ65C02::testCarryFlagDecimal(uint16_t n)                       { this->flagC = n > 0x99;  }                               //In decimal mode carry flag is set if value is bigger than 0x99
void EZ65C02::testOverflowFlag(uint8_t n1, uint8_t n2, uint8_t r)    { this->flagV = ((n1 ^ r) & (n2 ^ r)) > 0x7F;            } //Sets overflow flag if both operand have the same sign and the result has a different one
void EZ65C02::testOverflowFlagSub(uint8_t n1, uint8_t n2, uint8_t r) { this->flagV = ((n1 ^ r) & 0x80) && ((n1 ^ n2) & 0x80); } //Sets overflow flag for subtractions


//Call this function inside an ISR to issue a reset interrupt inside the VPU
void EZ65C02::issueReset()
{
    this->interruptStatus |= 0x1;
}

//Call this function inside an ISR to  issue an IRQ inside the VPU
void EZ65C02::issueIRQ()
{
    this->interruptStatus |= 0x2;
}

//Call this function inside an ISR to issue a NMI inside the VPU
void EZ65C02::issueNMI()
{
    this->interruptStatus |= 0x4;
}

//Vpu initialization
void EZ65C02::init(uint16_t startAddress)
{
    this->regPC = startAddress;
    this->regSP = 0xFF;
    this->regA  = 0;
    this->regX  = 0;
    this->regY  = 0;
    this->flagN = false;
    this->flagV = false;
    this->flagD = false;
    this->flagI = false;
    this->flagZ = false;
    this->flagC = false;
    this->interruptStatus = 0;
 }

//Process Reset, NMI and IRQ interrupts
void EZ65C02::processInterrupts()
{
    if (this->interruptStatus)
    {
        //Process Reset with highest priority
        if (this->interruptStatus & 0x1)
        {
            this->pushStack16(this->regPC);
            this->pushStack((this->flagN << 7) | (this->flagV << 6) | (1 << 5) | (this->flagD << 3) | (this->flagI << 2) | (this->flagZ << 1) | this->flagC);
            this->regA = 0;
            this->regX = 0;
            this->regY = 0;
            this->regSP = 0xFF;
            this->flagN = this->flagV = this->flagD = this->flagI = this->flagZ = this->flagC = false;
            this->regPC = this->readMem16(RESET_VECTOR_ADDR);
            this->interruptStatus = 0;
        }
        else
        {
            //Process IRQ if interrupts are enabled
            if (this->interruptStatus && !this->flagI)
            {
                this->pushStack16(this->regPC);
                this->pushStack((this->flagN << 7) | (this->flagV << 6) | (1 << 5) | (this->flagD << 3) | (this->flagI << 2) | (this->flagZ << 1) | this->flagC);
                this->flagI = true;
                this->flagD = false;
                this->regPC = this->readMem16(IRQ_VECTOR_ADDR);
            }

            this->interruptStatus &= 0xFB;

            //Process NMI. If Both IRQ and NMI happend in the same cycle, NMI vector is pushed last, so it will be executed before IRQ
            if (this->interruptStatus & 0x2)
            {
                this->pushStack16(this->regPC);
                this->pushStack((this->flagN << 7) | (this->flagV << 6) | (1 << 5) | (this->flagD << 3) | (this->flagI << 2) | (this->flagZ << 1) | this->flagC);
                this->flagI = true;
                this->flagD = false;
                this->regPC = this->readMem16(NMI_VECTOR_ADDR);
                this->interruptStatus &= 0xFD;
            }
        }
    }
}

//Increments the program counter and executes the next instruction
void EZ65C02::execCycle()
{
    uint8_t opcode;

    //Check for interrupts
    this->processInterrupts();

    //Fetch
    opcode = this->readMem(this->regPC);

    ++this->regPC;

    //Decode
    switch (opcode)
    {
        //Execute
        case 0x00: this->opcodeBRK_imp();                                break;
        case 0x01: this->opcodeORA_mem(this->getAddrIndirectX());        break;
        case 0x02: this->opcodeNOP_imp(1);                               break;
        case 0x03: this->opcodeNOP_imp(0);                               break;
        case 0x04: this->opcodeTSB_mem(this->getAddrZeroPage());         break;
        case 0x05: this->opcodeORA_mem(this->getAddrZeroPage());         break;
        case 0x06: this->opcodeASL_mem(this->getAddrZeroPage());         break;
        case 0x07: this->opcodeRMB_mem(this->getAddrZeroPage(), 0xFE);   break;
        case 0x08: this->opcodePHP_imp();                                break;
        case 0x09: this->opcodeORA_mem(this->getAddrImmediate());        break;
        case 0x0A: this->opcodeASL_acc();                                break;
        case 0x0B: this->opcodeNOP_imp(0);                               break;
        case 0x0C: this->opcodeTSB_mem(this->getAddrAbsolute());         break;
        case 0x0D: this->opcodeORA_mem(this->getAddrAbsolute());         break;
        case 0x0E: this->opcodeASL_mem(this->getAddrAbsolute());         break;
        case 0x0F: this->opcodeBBR_mem(this->getAddrZeroPage(), 0x01);   break;
        case 0x10: this->opcodeBPL_mem();                                break;
        case 0x11: this->opcodeORA_mem(this->getAddrIndirectY());        break;
        case 0x12: this->opcodeORA_mem(this->getAddrIndirectZeroPage()); break;
        case 0x13: this->opcodeNOP_imp(0);                               break;
        case 0x14: this->opcodeTRB_mem(this->getAddrZeroPage());         break;
        case 0x15: this->opcodeORA_mem(this->getAddrZeroPageX());        break;
        case 0x16: this->opcodeASL_mem(this->getAddrZeroPageX());        break;
        case 0x17: this->opcodeRMB_mem(this->getAddrZeroPage(), 0xFD);   break;
        case 0x18: this->opcodeCLC_imp();                                break;
        case 0x19: this->opcodeORA_mem(this->getAddrAbsoluteY());        break;
        case 0x1A: this->opcodeINC_acc();                                break;
        case 0x1B: this->opcodeNOP_imp(0);                               break;
        case 0x1C: this->opcodeTRB_mem(this->getAddrAbsolute());         break;
        case 0x1D: this->opcodeORA_mem(this->getAddrAbsoluteX());        break;
        case 0x1E: this->opcodeASL_mem(this->getAddrAbsoluteX());        break;
        case 0x1F: this->opcodeBBR_mem(this->getAddrZeroPage(), 0x02);   break;
        case 0x20: this->opcodeJSR_mem(this->getAddrAbsolute());         break;
        case 0x21: this->opcodeAND_mem(this->getAddrIndirectX());        break;
        case 0x22: this->opcodeNOP_imp(1);                               break;
        case 0x23: this->opcodeNOP_imp(0);                               break;
        case 0x24: this->opcodeBIT_mem(this->getAddrZeroPage());         break;
        case 0x25: this->opcodeAND_mem(this->getAddrZeroPage());         break;
        case 0x26: this->opcodeROL_mem(this->getAddrZeroPage());         break;
        case 0x27: this->opcodeRMB_mem(this->getAddrZeroPage(), 0xFB);   break;
        case 0x28: this->opcodePLP_imp();                                break;
        case 0x29: this->opcodeAND_mem(this->getAddrImmediate());        break;
        case 0x2A: this->opcodeROL_acc();                                break;
        case 0x2B: this->opcodeNOP_imp(0);                               break;
        case 0x2C: this->opcodeBIT_mem(this->getAddrAbsolute());         break;
        case 0x2D: this->opcodeAND_mem(this->getAddrAbsolute());         break;
        case 0x2E: this->opcodeROL_mem(this->getAddrAbsolute());         break;
        case 0x2F: this->opcodeBBR_mem(this->getAddrZeroPage(), 0x04);   break;
        case 0x30: this->opcodeBMI_mem();                                break;
        case 0x31: this->opcodeAND_mem(this->getAddrIndirectY());        break;
        case 0x32: this->opcodeAND_mem(this->getAddrIndirectZeroPage()); break;
        case 0x33: this->opcodeNOP_imp(0);                               break;
        case 0x34: this->opcodeBIT_mem(this->getAddrZeroPageX());        break;
        case 0x35: this->opcodeAND_mem(this->getAddrZeroPageX());        break;
        case 0x36: this->opcodeROL_mem(this->getAddrZeroPageX());        break;
        case 0x37: this->opcodeRMB_mem(this->getAddrZeroPage(), 0xF7);   break;
        case 0x38: this->opcodeSEC_imp();                                break;
        case 0x39: this->opcodeAND_mem(this->getAddrAbsoluteY());        break;
        case 0x3A: this->opcodeDEC_acc();                                break;
        case 0x3B: this->opcodeNOP_imp(0);                               break;
        case 0x3C: this->opcodeBIT_mem(this->getAddrAbsoluteX());        break;
        case 0x3D: this->opcodeAND_mem(this->getAddrAbsoluteX());        break;
        case 0x3E: this->opcodeROL_mem(this->getAddrAbsoluteX());        break;
        case 0x3F: this->opcodeBBR_mem(this->getAddrZeroPage(), 0x08);   break;
        case 0x40: this->opcodeRTI_imp();                                break;
        case 0x41: this->opcodeEOR_mem(this->getAddrIndirectX());        break;
        case 0x42: this->opcodeHLI_ext();                                break;
        case 0x43: this->opcodeNOP_imp(0);                               break;
        case 0x44: this->opcodeNOP_imp(1);                               break;
        case 0x45: this->opcodeEOR_mem(this->getAddrZeroPage());         break;
        case 0x46: this->opcodeLSR_mem(this->getAddrZeroPage());         break;
        case 0x47: this->opcodeRMB_mem(this->getAddrZeroPage(), 0xEF);   break;
        case 0x48: this->opcodePHA_imp();                                break;
        case 0x49: this->opcodeEOR_mem(this->getAddrImmediate());        break;
        case 0x4A: this->opcodeLSR_acc();                                break;
        case 0x4B: this->opcodeNOP_imp(0);                               break;
        case 0x4C: this->opcodeJMP_mem(this->getAddrAbsolute());         break;
        case 0x4D: this->opcodeEOR_mem(this->getAddrAbsolute());         break;
        case 0x4E: this->opcodeLSR_mem(this->getAddrAbsolute());         break;
        case 0x4F: this->opcodeBBR_mem(this->getAddrZeroPage(), 0x10);   break;
        case 0x50: this->opcodeBVC_mem();                                break;
        case 0x51: this->opcodeEOR_mem(this->getAddrIndirectY());        break;
        case 0x52: this->opcodeEOR_mem(this->getAddrIndirectZeroPage()); break;
        case 0x53: this->opcodeNOP_imp(0);                               break;
        case 0x54: this->opcodeNOP_imp(1);                               break;
        case 0x55: this->opcodeEOR_mem(this->getAddrZeroPageX());        break;
        case 0x56: this->opcodeLSR_mem(this->getAddrZeroPageX());        break;
        case 0x57: this->opcodeRMB_mem(this->getAddrZeroPage(), 0xDF);   break;
        case 0x58: this->opcodeCLI_imp();                                break;
        case 0x59: this->opcodeEOR_mem(this->getAddrAbsoluteY());        break;
        case 0x5A: this->opcodePHY_imp();                                break;
        case 0x5B: this->opcodeNOP_imp(0);                               break;
        case 0x5C: this->opcodeNOP_imp(2);                               break;
        case 0x5D: this->opcodeEOR_mem(this->getAddrAbsoluteX());        break;
        case 0x5E: this->opcodeLSR_mem(this->getAddrAbsoluteX());        break;
        case 0x5F: this->opcodeBBR_mem(this->getAddrZeroPage(), 0x20);   break;
        case 0x60: this->opcodeRTS_imp();                                break;
        case 0x61: this->opcodeADC_mem(this->getAddrIndirectX());        break;
        case 0x62: this->opcodeNOP_imp(1);                               break;
        case 0x63: this->opcodeNOP_imp(0);                               break;
        case 0x64: this->opcodeSTZ_mem(this->getAddrZeroPage());         break;
        case 0x65: this->opcodeADC_mem(this->getAddrZeroPage());         break;
        case 0x66: this->opcodeROR_mem(this->getAddrZeroPage());         break;
        case 0x67: this->opcodeRMB_mem(this->getAddrZeroPage(), 0xBF);   break;
        case 0x68: this->opcodePLA_imp();                                break;
        case 0x69: this->opcodeADC_mem(this->getAddrImmediate());        break;
        case 0x6A: this->opcodeROR_acc();                                break;
        case 0x6B: this->opcodeNOP_imp(0);                               break;
        case 0x6C: this->opcodeJMP_mem(this->getAddrIndirect());         break;
        case 0x6D: this->opcodeADC_mem(this->getAddrAbsolute());         break;
        case 0x6E: this->opcodeROR_mem(this->getAddrAbsolute());         break;
        case 0x6F: this->opcodeBBR_mem(this->getAddrZeroPage(), 0x40);   break;
        case 0x70: this->opcodeBVS_mem();                                break;
        case 0x71: this->opcodeADC_mem(this->getAddrIndirectY());        break;
        case 0x72: this->opcodeADC_mem(this->getAddrIndirectZeroPage()); break;
        case 0x73: this->opcodeNOP_imp(0);                               break;
        case 0x74: this->opcodeSTZ_mem(this->getAddrZeroPageX());        break;
        case 0x75: this->opcodeADC_mem(this->getAddrZeroPageX());        break;
        case 0x76: this->opcodeROR_mem(this->getAddrZeroPageX());        break;
        case 0x77: this->opcodeRMB_mem(this->getAddrZeroPage(), 0x7F);   break;
        case 0x78: this->opcodeSEI_imp();                                break;
        case 0x79: this->opcodeADC_mem(this->getAddrAbsoluteY());        break;
        case 0x7A: this->opcodePLY_imp();                                break;
        case 0x7B: this->opcodeNOP_imp(0);                               break;
        case 0x7C: this->opcodeJMP_mem(this->getAddrIndirectAbsX());     break;
        case 0x7D: this->opcodeADC_mem(this->getAddrAbsoluteX());        break;
        case 0x7E: this->opcodeROR_mem(this->getAddrAbsoluteX());        break;
        case 0x7F: this->opcodeBBR_mem(this->getAddrZeroPage(), 0x80);   break;
        case 0x80: this->opcodeBRA_mem();                                break;
        case 0x81: this->opcodeSTA_mem(this->getAddrIndirectX());        break;
        case 0x82: this->opcodeNOP_imp(1);                               break;
        case 0x83: this->opcodeNOP_imp(0);                               break;
        case 0x84: this->opcodeSTY_mem(this->getAddrZeroPage());         break;
        case 0x85: this->opcodeSTA_mem(this->getAddrZeroPage());         break;
        case 0x86: this->opcodeSTX_mem(this->getAddrZeroPage());         break;
        case 0x87: this->opcodeSMB_mem(this->getAddrZeroPage(), 0x01);   break;
        case 0x88: this->opcodeDEY_imp();                                break;
        case 0x89: this->opcodeBIT_imm();                                break;
        case 0x8A: this->opcodeTXA_imp();                                break;
        case 0x8B: this->opcodeNOP_imp(0);                               break;
        case 0x8C: this->opcodeSTY_mem(this->getAddrAbsolute());         break;
        case 0x8D: this->opcodeSTA_mem(this->getAddrAbsolute());         break;
        case 0x8E: this->opcodeSTX_mem(this->getAddrAbsolute());         break;
        case 0x8F: this->opcodeBBS_mem(this->getAddrZeroPage(), 0x01);   break;
        case 0x90: this->opcodeBCC_mem();                                break;
        case 0x91: this->opcodeSTA_mem(this->getAddrIndirectY());        break;
        case 0x92: this->opcodeSTA_mem(this->getAddrIndirectZeroPage()); break;
        case 0x93: this->opcodeNOP_imp(0);                               break;
        case 0x94: this->opcodeSTY_mem(this->getAddrZeroPageX());        break;
        case 0x95: this->opcodeSTA_mem(this->getAddrZeroPageX());        break;
        case 0x96: this->opcodeSTX_mem(this->getAddrZeroPageY());        break;
        case 0x97: this->opcodeSMB_mem(this->getAddrZeroPage(), 0x02);   break;
        case 0x98: this->opcodeTYA_imp();                                break;
        case 0x99: this->opcodeSTA_mem(this->getAddrAbsoluteY());        break;
        case 0x9A: this->opcodeTXS_imp();                                break;
        case 0x9B: this->opcodeNOP_imp(0);                               break;
        case 0x9C: this->opcodeSTZ_mem(this->getAddrAbsolute());         break;
        case 0x9D: this->opcodeSTA_mem(this->getAddrAbsoluteX());        break;
        case 0x9E: this->opcodeSTZ_mem(this->getAddrAbsoluteX());        break;
        case 0x9F: this->opcodeBBS_mem(this->getAddrZeroPage(), 0x02);   break;
        case 0xA0: this->opcodeLDY_mem(this->getAddrImmediate());        break;
        case 0xA1: this->opcodeLDA_mem(this->getAddrIndirectX());        break;
        case 0xA2: this->opcodeLDX_mem(this->getAddrImmediate());        break;
        case 0xA3: this->opcodeNOP_imp(0);                               break;
        case 0xA4: this->opcodeLDY_mem(this->getAddrZeroPage());         break;
        case 0xA5: this->opcodeLDA_mem(this->getAddrZeroPage());         break;
        case 0xA6: this->opcodeLDX_mem(this->getAddrZeroPage());         break;
        case 0xA7: this->opcodeSMB_mem(this->getAddrZeroPage(), 0x04);   break;
        case 0xA8: this->opcodeTAY_imp();                                break;
        case 0xA9: this->opcodeLDA_mem(this->getAddrImmediate());        break;
        case 0xAA: this->opcodeTAX_imp();                                break;
        case 0xAB: this->opcodeNOP_imp(0);                               break;
        case 0xAC: this->opcodeLDY_mem(this->getAddrAbsolute());         break;
        case 0xAD: this->opcodeLDA_mem(this->getAddrAbsolute());         break;
        case 0xAE: this->opcodeLDX_mem(this->getAddrAbsolute());         break;
        case 0xAF: this->opcodeBBS_mem(this->getAddrZeroPage(), 0x04);   break;
        case 0xB0: this->opcodeBCS_mem();                                break;
        case 0xB1: this->opcodeLDA_mem(this->getAddrIndirectY());        break;
        case 0xB2: this->opcodeLDA_mem(this->getAddrIndirectZeroPage()); break;
        case 0xB3: this->opcodeNOP_imp(0);                               break;
        case 0xB4: this->opcodeLDY_mem(this->getAddrZeroPageX());        break;
        case 0xB5: this->opcodeLDA_mem(this->getAddrZeroPageX());        break;
        case 0xB6: this->opcodeLDX_mem(this->getAddrZeroPageY());        break;
        case 0xB7: this->opcodeSMB_mem(this->getAddrZeroPage(), 0x08);   break;
        case 0xB8: this->opcodeCLV_imp();                                break;
        case 0xB9: this->opcodeLDA_mem(this->getAddrAbsoluteY());        break;
        case 0xBA: this->opcodeTSX_imp();                                break;
        case 0xBB: this->opcodeNOP_imp(0);                               break;
        case 0xBC: this->opcodeLDY_mem(this->getAddrAbsoluteX());        break;
        case 0xBD: this->opcodeLDA_mem(this->getAddrAbsoluteX());        break;
        case 0xBE: this->opcodeLDX_mem(this->getAddrAbsoluteY());        break;
        case 0xBF: this->opcodeBBS_mem(this->getAddrZeroPage(), 0x08);   break;
        case 0xC0: this->opcodeCPY_mem(this->getAddrImmediate());        break;
        case 0xC1: this->opcodeCMP_mem(this->getAddrIndirectX());        break;
        case 0xC2: this->opcodeNOP_imp(1);                               break;
        case 0xC3: this->opcodeNOP_imp(0);                               break;
        case 0xC4: this->opcodeCPY_mem(this->getAddrZeroPage());         break;
        case 0xC5: this->opcodeCMP_mem(this->getAddrZeroPage());         break;
        case 0xC6: this->opcodeDEC_mem(this->getAddrZeroPage());         break;
        case 0xC7: this->opcodeSMB_mem(this->getAddrZeroPage(), 0x10);   break;
        case 0xC8: this->opcodeINY_imp();                                break;
        case 0xC9: this->opcodeCMP_mem(this->getAddrImmediate());        break;
        case 0xCA: this->opcodeDEX_imp();                                break;
        case 0xCB: this->opcodeWAI_imp();                                break;
        case 0xCC: this->opcodeCPY_mem(this->getAddrAbsolute());         break;
        case 0xCD: this->opcodeCMP_mem(this->getAddrAbsolute());         break;
        case 0xCE: this->opcodeDEC_mem(this->getAddrAbsolute());         break;
        case 0xCF: this->opcodeBBS_mem(this->getAddrZeroPage(), 0x10);   break;
        case 0xD0: this->opcodeBNE_mem();                                break;
        case 0xD1: this->opcodeCMP_mem(this->getAddrIndirectY());        break;
        case 0xD2: this->opcodeCMP_mem(this->getAddrIndirectZeroPage()); break;
        case 0xD3: this->opcodeNOP_imp(0);                               break;
        case 0xD4: this->opcodeNOP_imp(1);                               break;
        case 0xD5: this->opcodeCMP_mem(this->getAddrZeroPageX());        break;
        case 0xD6: this->opcodeDEC_mem(this->getAddrZeroPageX());        break;
        case 0xD7: this->opcodeSMB_mem(this->getAddrZeroPage(), 0x20);   break;
        case 0xD8: this->opcodeCLD_imp();                                break;
        case 0xD9: this->opcodeCMP_mem(this->getAddrAbsoluteY());        break;
        case 0xDA: this->opcodePHX_imp();                                break;
        case 0xDB: this->opcodeSTP_imp();                                break;
        case 0xDC: this->opcodeNOP_imp(2);                               break;
        case 0xDD: this->opcodeCMP_mem(this->getAddrAbsoluteX());        break;
        case 0xDE: this->opcodeDEC_mem(this->getAddrAbsoluteX());        break;
        case 0xDF: this->opcodeBBS_mem(this->getAddrZeroPage(), 0x20);   break;
        case 0xE0: this->opcodeCPX_mem(this->getAddrImmediate());        break;
        case 0xE1: this->opcodeSBC_mem(this->getAddrIndirectX());        break;
        case 0xE2: this->opcodeNOP_imp(1);                               break;
        case 0xE3: this->opcodeNOP_imp(0);                               break;
        case 0xE4: this->opcodeCPX_mem(this->getAddrZeroPage());         break;
        case 0xE5: this->opcodeSBC_mem(this->getAddrZeroPage());         break;
        case 0xE6: this->opcodeINC_mem(this->getAddrZeroPage());         break;
        case 0xE7: this->opcodeSMB_mem(this->getAddrZeroPage(), 0x40);   break;
        case 0xE8: this->opcodeINX_imp();                                break;
        case 0xE9: this->opcodeSBC_mem(this->getAddrImmediate());        break;
        case 0xEA: this->opcodeNOP_imp(0);                               break;
        case 0xEB: this->opcodeNOP_imp(0);                               break;
        case 0xEC: this->opcodeCPX_mem(this->getAddrAbsolute());         break;
        case 0xED: this->opcodeSBC_mem(this->getAddrAbsolute());         break;
        case 0xEE: this->opcodeINC_mem(this->getAddrAbsolute());         break;
        case 0xEF: this->opcodeBBS_mem(this->getAddrZeroPage(), 0x40);   break;
        case 0xF0: this->opcodeBEQ_mem();                                break;
        case 0xF1: this->opcodeSBC_mem(this->getAddrIndirectY());        break;
        case 0xF2: this->opcodeSBC_mem(this->getAddrIndirectZeroPage()); break;
        case 0xF3: this->opcodeNOP_imp(0);                               break;
        case 0xF4: this->opcodeNOP_imp(1);                               break;
        case 0xF5: this->opcodeSBC_mem(this->getAddrZeroPageX());        break;
        case 0xF6: this->opcodeINC_mem(this->getAddrZeroPageX());        break;
        case 0xF7: this->opcodeSMB_mem(this->getAddrZeroPage(), 0x80);   break;
        case 0xF8: this->opcodeSED_imp();                                break;
        case 0xF9: this->opcodeSBC_mem(this->getAddrAbsoluteY());        break;
        case 0xFA: this->opcodePLX_imp();                                break;
        case 0xFB: this->opcodeNOP_imp(0);                               break;
        case 0xFC: this->opcodeNOP_imp(2);                               break;
        case 0xFD: this->opcodeSBC_mem(this->getAddrAbsoluteX());        break;
        case 0xFE: this->opcodeINC_mem(this->getAddrAbsoluteX());        break;
        case 0xFF: this->opcodeBBS_mem(this->getAddrZeroPage(), 0x80);   break;
    }
}


//Opcodes Handlers

//Add with carry
void EZ65C02::opcodeADC_mem(uint16_t address)
{
    uint8_t value   = this->readMem(address);
    uint16_t result = value + this->regA + this->flagC;

    this->testOverflowFlag(this->regA, value, (uint8_t)result);

    if (!this->flagD)
    {
        this->testCarryFlag(result);
    }
    else
    {
        if (((this->regA & 0xF) + (value & 0xF) + this->flagC) > 0x09)
        {
            result += 0x06;
        }

        this->testCarryFlagDecimal(result);

        if (this->flagC)
        {
            result += 0x60;
        }
    }

    this->regA = (uint8_t)result;

    this->testZeroFlag(this->regA);
    this->testNegativeFlag(this->regA);
}

//Logical AND
void EZ65C02::opcodeAND_mem(uint16_t address)
{
    this->regA &= this->readMem(address);

    this->testZeroFlag(this->regA);
    this->testNegativeFlag(this->regA);
}

//Aritmetic shift left accumulator
void EZ65C02::opcodeASL_acc()
{
    this->testCarryFlagShiftLeft(this->regA);

    this->regA = this->regA << 1;

    this->testZeroFlag(this->regA);
    this->testNegativeFlag(this->regA);
}

//Aritmetic shift left memory
void EZ65C02::opcodeASL_mem(uint16_t address)
{
    uint8_t value = this->readMem(address);

    this->testCarryFlagShiftLeft(value);

    value = value << 1;

    this->testZeroFlag(value);
    this->testNegativeFlag(value);

    this->writeMem(address, value);
}

//Branch if bit in memory is clear
void EZ65C02::opcodeBBR_mem(uint16_t address, uint8_t bitNumber)
{
    uint8_t value = this->readMem(address);

    if (!(value & bitNumber))
    {
        this->regPC = this->getAddrRelative();
    }
    else
    {
        ++this->regPC;
    }
}

//Branch if bit in memory is set
void EZ65C02::opcodeBBS_mem(uint16_t address, uint8_t bitNumber)
{
    uint8_t value = this->readMem(address);

    if (value & bitNumber)
    {
        this->regPC = this->getAddrRelative();
    }
    else
    {
        ++this->regPC;
    }
}

//Branch if carry clear
void EZ65C02::opcodeBCC_mem()
{
    if (!this->flagC)
    {
        this->regPC = getAddrRelative();
    }
    else
    {
        ++this->regPC;
    }
}

//Branch if carry set
void EZ65C02::opcodeBCS_mem()
{
    if (this->flagC)
    {
        this->regPC = getAddrRelative();
    }
    else
    {
        ++this->regPC;
    }
}

//Branch if equal
void EZ65C02::opcodeBEQ_mem()
{
    if (this->flagZ)
    {
        this->regPC = getAddrRelative();
    }
    else
    {
        ++this->regPC;
    }
}

//Bitwise AND memory with accumulator with immediate address (65C02 variant that sets only flag Z)
void EZ65C02::opcodeBIT_imm()
{
    this->flagZ = (this->readMem(this->getAddrImmediate()) & this->regA) == 0;
}

//Bitwise AND memory with accumulator (but only set flags N, V and Z)
void EZ65C02::opcodeBIT_mem(uint16_t address)
{
    uint8_t value = this->readMem(address);

    this->flagN = (value & 0x80) != 0;
    this->flagV = (value & 0x40) != 0;
    this->flagZ = (value & this->regA) == 0;
}

//Branch if negative flag is set
void EZ65C02::opcodeBMI_mem()
{
    if (this->flagN)
    {
        this->regPC = this->getAddrRelative();
    }
    else
    {
        ++this->regPC;
    }
}

//Branch if not equal
void EZ65C02::opcodeBNE_mem()
{
    if (!this->flagZ)
    {
        this->regPC = this->getAddrRelative();
    }
    else
    {
        ++this->regPC;
    }
}

//Branch if positive
void EZ65C02::opcodeBPL_mem()
{
    if (!this->flagN)
    {
        this->regPC = this->getAddrRelative();
    }
    else
    {
        ++this->regPC;
    }
}

//Branch always
void EZ65C02::opcodeBRA_mem()
{
    this->regPC = this->getAddrRelative();
}

//Break
void EZ65C02::opcodeBRK_imp()
{
    ++this->regPC;
    this->pushStack16(this->regPC);
    this->pushStack((this->flagN << 7) | (this->flagV << 6) | (1 << 5) | (1 << 4) | (this->flagD << 3) | (this->flagI << 2) | (this->flagZ << 1) | this->flagC);
    this->flagI = true;
    this->flagD = false;
    this->regPC = this->readMem16(IRQ_VECTOR_ADDR);
}

//Branch if overflow flag is clear
void EZ65C02::opcodeBVC_mem()
{
    if (!this->flagV)
    {
        this->regPC = this->getAddrRelative();
    }
    else
    {
        ++this->regPC;
    }
}

//Branch if overflow flag is set
void EZ65C02::opcodeBVS_mem()
{
    if (this->flagV)
    {
        this->regPC = this->getAddrRelative();
    }
    else
    {
        ++this->regPC;
    }
}

//Clear carry flag
void EZ65C02::opcodeCLC_imp()
{
    this->flagC = false;
}

//Clear decimal flag
void EZ65C02::opcodeCLD_imp()
{
    this->flagD = false;
}

//Clear interrupt disable flag
void EZ65C02::opcodeCLI_imp()
{
    this->flagI = false;
}

//Clear overflow flag
void EZ65C02::opcodeCLV_imp()
{
    this->flagV = false;
}

//Compare accumulator with memory
void EZ65C02::opcodeCMP_mem(uint16_t address)
{
    uint16_t value = this->regA - this->readMem(address);

    this->testCarryFlagSub(value);
    this->testNegativeFlag((int8_t )value);
    this->testZeroFlag((uint8_t)value);
}

//Compare register X with memory
void EZ65C02::opcodeCPX_mem(uint16_t address)
{
    uint16_t value = this->regX - this->readMem(address);

    this->testCarryFlagSub(value);
    this->testNegativeFlag((int8_t )value);
    this->testZeroFlag((uint8_t)value);
}

//Compare register Y with memory
void EZ65C02::opcodeCPY_mem(uint16_t address)
{
    uint16_t value = this->regY - this->readMem(address);

    this->testCarryFlagSub(value);
    this->testNegativeFlag((int8_t )value);
    this->testZeroFlag((uint8_t)value);
}

//Decrement accumulator
void EZ65C02::opcodeDEC_acc()
{
    --this->regA;

    this->testZeroFlag(this->regA);
    this->testNegativeFlag(this->regA);
}

//Decrement in memory
void EZ65C02::opcodeDEC_mem(uint16_t address)
{
    uint8_t value = this->readMem(address) - 1;

    this->testZeroFlag(value);
    this->testNegativeFlag(value);

    this->writeMem(address, value);
}

//Decrement register X
void EZ65C02::opcodeDEX_imp()
{
    --this->regX;
    this->testNegativeFlag(this->regX);
    this->testZeroFlag(this->regX);
}

//Decrement register Y
void EZ65C02::opcodeDEY_imp()
{
    --this->regY;

    this->testZeroFlag(this->regY);
    this->testNegativeFlag(this->regY);
}

//Exclusive OR
void EZ65C02::opcodeEOR_mem(uint16_t address)
{
    this->regA ^= this->readMem(address);

    this->testNegativeFlag(this->regA);
    this->testZeroFlag(this->regA);
}

//Increment accumulator
void EZ65C02::opcodeINC_acc()
{
    ++this->regA;

    this->testZeroFlag(this->regA);
    this->testNegativeFlag(this->regA);
}

//Increment in memory
void EZ65C02::opcodeINC_mem(uint16_t address)
{
    uint8_t value = this->readMem(address) + 1;

    this->testZeroFlag(value);
    this->testNegativeFlag(value);

    this->writeMem(address, value);
}

//Increment register X
void EZ65C02::opcodeINX_imp()
{
    ++this->regX;

    this->testZeroFlag(this->regX);
    this->testNegativeFlag(this->regX);
}

//Increment register Y
void EZ65C02::opcodeINY_imp()
{
    ++this->regY;

    this->testNegativeFlag(this->regY);
    this->testZeroFlag(this->regY);
}

//Unconditional jump
void EZ65C02::opcodeJMP_mem(uint16_t address)
{
    this->regPC = address;
}

//Jump to subroutine
void EZ65C02::opcodeJSR_mem(uint16_t address)
{
    this->pushStack16(this->regPC - 1);
    this->regPC = address;
}

//Load accumulator
void EZ65C02::opcodeLDA_mem(uint16_t address)
{
    this->regA = this->readMem(address);
    this->testNegativeFlag(this->regA);
    this->testZeroFlag(this->regA);
}

//Load register X
void EZ65C02::opcodeLDX_mem(uint16_t address)
{
    this->regX = this->readMem(address);
    this->testNegativeFlag(this->regX);
    this->testZeroFlag(this->regX);
}

//Load register Y
void EZ65C02::opcodeLDY_mem(uint16_t address)
{
    this->regY = this->readMem(address);
    this->testNegativeFlag(this->regY);
    this->testZeroFlag(this->regY);
}

//Logical shift right accumulator
void EZ65C02::opcodeLSR_acc()
{
    this->testCarryFlagShiftRight(this->regA);

    this->regA = this->regA >> 1;

    this->testZeroFlag(this->regA);
    this->testNegativeFlag(this->regA);
}

//Logical shift right in memory
void EZ65C02::opcodeLSR_mem(uint16_t address)
{
    uint8_t value = this->readMem(address);

    this->testCarryFlagShiftRight(value);

    value = value >> 1;

    this->testZeroFlag(value);
    this->testNegativeFlag(value);

    this->writeMem(address, value);
}

//No operation
void EZ65C02::opcodeNOP_imp(uint8_t paddingBytes)
{
    this->regPC += paddingBytes;
}

//Inclusive OR
void EZ65C02::opcodeORA_mem(uint16_t address)
{
    this->regA |= this->readMem(address);

    this->testNegativeFlag(this->regA);
    this->testZeroFlag(this->regA);
}

//Push accumulator in to the stack
void EZ65C02::opcodePHA_imp()
{
    this->pushStack(this->regA);
}

//Push status register into the stack
void EZ65C02::opcodePHP_imp()
{
    this->pushStack((this->flagN << 7) | (this->flagV << 6) | (1 << 5) | (1 << 4) | (this->flagD << 3) | (this->flagI << 2) | (this->flagZ << 1) | this->flagC);
}

//Push register X into the stack
void EZ65C02::opcodePHX_imp()
{
    this->pushStack(this->regX);
}

//Push register Y into the stack
void EZ65C02::opcodePHY_imp()
{
    this->pushStack(this->regY);
}

//Pull accumulator from stack
void EZ65C02::opcodePLA_imp()
{
    this->regA = this->pullStack();

    this->testZeroFlag(this->regA);
    this->testNegativeFlag(this->regA);
}

//Pull status register from the stack
void EZ65C02::opcodePLP_imp()
{
    uint8_t status = pullStack();

    this->flagN = (status & 0x80) != 0;
    this->flagV = (status & 0x40) != 0;
    this->flagD = (status & 0x08) != 0;
    this->flagI = (status & 0x04) != 0;
    this->flagZ = (status & 0x02) != 0;
    this->flagC = (status & 0x01);
}

//Pull register X from the stack
void EZ65C02::opcodePLX_imp()
{
    this->regX = this->pullStack();

    this->testZeroFlag(this->regX);
    this->testNegativeFlag(this->regX);
}

//Pull register Y from the stack
void EZ65C02::opcodePLY_imp()
{
    this->regY = this->pullStack();

    this->testZeroFlag(this->regY);
    this->testNegativeFlag(this->regY);
}

//Reset a specified bit in memory
void EZ65C02::opcodeRMB_mem(uint16_t address, uint8_t bitNumber)
{
    uint8_t value = this->readMem(address);

    value &= bitNumber;

    this->writeMem(address, value);
}

//Rotate left accumulator
void EZ65C02::opcodeROL_acc()
{
    uint8_t oldA = this->regA;

    this->regA = (this->regA << 1) | this->flagC;

    this->testCarryFlagShiftLeft(oldA);
    this->testZeroFlag(this->regA);
    this->testNegativeFlag(this->regA);
}

//Rotate left in memory
void EZ65C02::opcodeROL_mem(uint16_t address)
{
    uint8_t value = this->readMem(address);
    uint8_t result = (value << 1) | this->flagC;

    this->testCarryFlagShiftLeft(value);
    this->testZeroFlag(result);
    this->testNegativeFlag(result);

    this->writeMem(address, result);
}

//Rotate left accumulator
void EZ65C02::opcodeROR_acc()
{
    uint8_t oldA = this->regA;

    this->regA = (this->regA >> 1) | (this->flagC << 7);

    this->testCarryFlagShiftRight(oldA);
    this->testZeroFlag(this->regA);
    this->testNegativeFlag(this->regA);
}

//Rotate left in memory
void EZ65C02::opcodeROR_mem(uint16_t address)
{
    uint8_t value = this->readMem(address);
    uint8_t result = (value >> 1) | (this->flagC << 7);

    this->testCarryFlagShiftRight(value);
    this->testZeroFlag(result);
    this->testNegativeFlag(result);

    this->writeMem(address, result);
}

//Return from interrupt routine
void EZ65C02::opcodeRTI_imp()
{
    uint8_t status = this->pullStack();

    this->flagN = (status & 0x80) != 0;
    this->flagV = (status & 0x40) != 0;
    this->flagD = (status & 0x08) != 0;
    this->flagI = (status & 0x04) != 0;
    this->flagZ = (status & 0x02) != 0;
    this->flagC = (status & 0x01);

    this->regPC = this->pullStack16();
}

//Return from subroutine
void EZ65C02::opcodeRTS_imp()
{
    this->regPC = this->pullStack16() + 1;
}

//Subtract with carry
void EZ65C02::opcodeSBC_mem(uint16_t address)
{
    uint8_t value = this->readMem(address);
    uint16_t result = this->regA - value - (this->flagC ^ 1);

    this->testOverflowFlagSub(this->regA, value, (uint8_t)result);

    if (!this->flagD)
    {
        this->testCarryFlagSub(result);
    }
    else
    {
        if (((this->regA & 0xF) - (this->flagC ^ 1)) < ((value & 0xF)))
        {
            result -= 0x06;
        }

        if (result > 0x99)
        {
            result -= 0x60;
        }

        this->testCarryFlagSub(result);
    }

    this->regA = (uint8_t)result;

    this->testZeroFlag(this->regA);
    this->testNegativeFlag(this->regA);
}

//Set carry flag
void EZ65C02::opcodeSEC_imp()
{
    this->flagC = true;
}

//Set decimal flag
void EZ65C02::opcodeSED_imp()
{
    this->flagD = true;
}

//Set interrupt disable flag
void EZ65C02::opcodeSEI_imp()
{
    this->flagI = true;
}

//Set a specified bit in memory
void EZ65C02::opcodeSMB_mem(uint16_t address, uint8_t bitNumber)
{
    uint8_t value = this->readMem(address);

    value |= bitNumber;

    this->writeMem(address, value);
}

//Store accumulator
void EZ65C02::opcodeSTA_mem(uint16_t address)
{
    this->writeMem(address, this->regA);
}

//Stop execution until a reset interrupt occurs
void EZ65C02::opcodeSTP_imp()
{
    while (this->interruptStatus != 0x1);
}

//Store register X
void EZ65C02::opcodeSTX_mem(uint16_t address)
{
    writeMem(address, this->regX);
}

//Store register Y
void EZ65C02::opcodeSTY_mem(uint16_t address)
{
    writeMem(address, this->regY);
}

//Store a zero value
void EZ65C02::opcodeSTZ_mem(uint16_t address)
{
    writeMem(address, 0);
}

//Copy accumulator to register X
void EZ65C02::opcodeTAX_imp()
{
    this->regX = this->regA;
    this->testNegativeFlag(this->regX);
    this->testZeroFlag(this->regX);
}

//Copy accumulator to register Y
void EZ65C02::opcodeTAY_imp()
{
    this->regY = this->regA;
    this->testNegativeFlag(this->regY);
    this->testZeroFlag(this->regY);
}

//Test the memory byte to see if it contains any of the bits indicated by the value in the accumulator, then the bits are reset in the memory byte.
void EZ65C02::opcodeTRB_mem(uint16_t address)
{
    uint8_t value = this->readMem(address);

    this->flagZ = (this->regA & value) == 0;

    value &= ~this->regA;

    this->writeMem(address, value);
}

//Test the memory byte to see if it contains any of the bits indicated by the value in the accumulator, then the bits are set in the memory byte.
void EZ65C02::opcodeTSB_mem(uint16_t address)
{
    uint8_t value = readMem(address);

    this->flagZ = (this->regA & value) == 0;

    value |= this->regA;

    this->writeMem(address, value);
}

//Copy stack pointer to register X
void EZ65C02::opcodeTSX_imp()
{
    this->regX = this->regSP;
    this->testNegativeFlag(this->regX);
    this->testZeroFlag(this->regX);
}

//Copy register X to accumulator
void EZ65C02::opcodeTXA_imp()
{
    this->regA = this->regX;
    this->testNegativeFlag(this->regA);
    this->testZeroFlag(this->regA);
}

//Copy register X to stack pointer
void EZ65C02::opcodeTXS_imp()
{
    this->regSP = this->regX;
}

//Copy register Y to accumulator
void EZ65C02::opcodeTYA_imp()
{
    this->regA = this->regY;
    this->testNegativeFlag(this->regA);
    this->testZeroFlag(this->regA);
}

//Waits of interrupts. If flag I is set IRQs still restore execution but no ISR will be executed
void EZ65C02::opcodeWAI_imp()
{
    while (this->interruptStatus == 0);
}
