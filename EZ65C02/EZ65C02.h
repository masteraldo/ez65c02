/*
   _____ _______  ____   ____ ___ ____
  | ____|__  / /_| ___| / ___/ _ \___ \
  |  _|   / / '_ \___ \| |  | | | |__) |
  | |___ / /| (_) |__) | |__| |_| / __/
  |_____/____\___/____/ \____\___/_____| P R O J E C T

A WDC65C02 emulator designed for simplicity, portability and extensivity.
It aims to be an opcode interpreter (not cycle accurate) for MCUs and it's the emulator core of the ZerOS project.
WDM opcode is used to provide additional high level routines.

(c) 2021 Eraldo Zanon - EZ65C02 Project
This code is licensed under MIT license (see LICENSE.txt for details)
*/

#ifndef EZ65C02_H
#define EZ65C02_H

constexpr uint16_t DEFAULT_PC_START_ADDR = 0x400;  //The default address at which the vpu will start fetching code
constexpr uint16_t STACK_BOTTOM_ADDR     = 0x100;  //256 Stack first address inside the emulated cpu
constexpr uint16_t STACK_TOP_ADDR        = 0x1FF;  //511 Stack last address inside the emulated cpu
constexpr uint16_t NMI_VECTOR_ADDR       = 0xFFFA; //Address where to find the NMI ISR
constexpr uint16_t RESET_VECTOR_ADDR     = 0xFFFC; //Address the PC points to on power on / reset
constexpr uint16_t IRQ_VECTOR_ADDR       = 0xFFFE; //Address where to find the IRQ ISR
constexpr uint16_t INT_MEM_TOP_ADDR      = 0xFFFF; //The highest address that will give access to MCU internal memory. Addresses bigger than it will result in read/write to the external memory
constexpr uint16_t INT_MEM_MIN_SIZE      = 512;    //The minimum amount of MCU internal memory needed to start the VPU: 0-255 for ZeroPage and 256-511 for Stack
constexpr uint32_t MAX_MEMORY_SIZE       = 65536;  //The maximum size of a program

//Type definitions
class EZ65C02
{
    public:
        //Main functions
        void init(uint16_t startAddress);   //Sets up registers to their initial value and sets the program counter to the specified starting address
        void execCycle();                   //Executes a single cycle of the cpu

        //Functions for interrupt handlers
		void issueReset();  //triggers a reset interrupt
		void issueIRQ();    //Triggers a normal interrupt
		void issueNMI();    //Triggers a non-maskable interrupt

        //Read / write functions to internal or external memory or to memory mapped devices (if implemented)
        uint8_t readMem(uint16_t address);              //Reads a byte from memory. The type of memory depends on the defined size of the internal and external memory
        void writeMem(uint16_t address, uint8_t data);  //Writes a byte to memory. The type of memory depends on the defined size of the internal and external memory

    private:
        uint8_t internalMemory[INT_MEM_TOP_ADDR + 1];   //The internal memory used for ZeroPage, Stack and the first bytes of ram. Must be at least 512 bytes to accomodate ZeroPage and Stack
        uint16_t startAddr; //The starting address the program counter is set on startup and reset
        uint16_t regPC;     //16 bit program counter
        uint8_t regSP;      //8 bit stack pointer
        uint8_t regA;       //8 bit accumulator
        uint8_t regX;       //8 bit X register
        uint8_t regY;       //8 bit Y register
        uint8_t flagN;      //Status register Sign flag
        uint8_t flagV;      //Status register Overflow flag
        uint8_t flagD;      //Status register Decimal Mode flag
        uint8_t flagI;      //Status register Disable Interrupts flag
        uint8_t flagZ;      //Status register Zero flag
        uint8_t flagC;      //Status register Carry flag
                            //flagB doesn't exist inside the 65C02 status register, it's set only when pushed on the stack by BRK or PHP

        volatile uint8_t  interruptStatus; //Set of 3 bits to handle external interrupts: 0 = No interrupts, 00000001 = RESET, 00000010 = NMI, 00000100 = IRQ

        //Check for raised interrupts before every cycle
        void processInterrupts();

        //Memory read functions
        uint8_t  readMemZeroPage(uint8_t  address);
        uint8_t  readExtMem(uint16_t  address);
        uint8_t  pullStack();
        uint16_t readMem16(uint16_t  address);
        uint16_t readMemZeroPage16(uint8_t  address);
        uint16_t readExtMem16(uint16_t  address);
        uint16_t pullStack16();

        //Memory write functions
        void writeMemZeroPage(uint8_t  address, uint8_t  data);
        void writeExtMem(uint16_t  address, uint8_t  data);
        void pushStack(uint8_t  data);
        void writeMem16(uint16_t  address, uint16_t  data);
        void writeMemZeroPage16(uint8_t  address, uint16_t  data);
        void writeExtMem16(uint16_t  address, uint16_t  data);
        void pushStack16(uint16_t  data);

        //Address calculation functions
        uint16_t getAddrImmediate();
        uint8_t  getAddrZeroPage();
        uint8_t  getAddrZeroPageX();
        uint8_t  getAddrZeroPageY();
        uint16_t getAddrRelative();
        uint16_t getAddrAbsolute();
        uint16_t getAddrAbsoluteX();
        uint16_t getAddrAbsoluteY();
        uint16_t getAddrIndirect();
        uint16_t getAddrIndirectZeroPage();
        uint16_t getAddrIndirectX();
        uint16_t getAddrIndirectY();
        uint16_t getAddrIndirectAbsX();

        //Status register manipulation functions
        void testZeroFlag(uint8_t  n);
        void testNegativeFlag(int8_t  n);
        void testCarryFlag(uint16_t  n);
        void testCarryFlagSub(uint16_t  n);
        void testCarryFlagShiftRight(uint8_t  n);
        void testCarryFlagShiftLeft(uint8_t  n);
        void testOverflowFlag(uint8_t  n1, uint8_t  n2, uint8_t  r);
        void testOverflowFlagSub(uint8_t n1, uint8_t n2, uint8_t r);
        void testCarryFlagDecimal(uint16_t  n);

        //Standard WDC65C02 opcodes
        void opcodeADC_mem(uint16_t  address);
        void opcodeAND_mem(uint16_t  address);
        void opcodeASL_acc();
        void opcodeASL_mem(uint16_t  address);
        void opcodeBBR_mem(uint16_t  address, uint8_t  bitNumber);
        void opcodeBBS_mem(uint16_t  address, uint8_t  bitNumber);
        void opcodeBCC_mem();
        void opcodeBCS_mem();
        void opcodeBEQ_mem();
        void opcodeBIT_imm();
        void opcodeBIT_mem(uint16_t  address);
        void opcodeBMI_mem();
        void opcodeBNE_mem();
        void opcodeBPL_mem();
        void opcodeBRA_mem();
        void opcodeBRK_imp();
        void opcodeBVC_mem();
        void opcodeBVS_mem();
        void opcodeCLC_imp();
        void opcodeCLD_imp();
        void opcodeCLI_imp();
        void opcodeCLV_imp();
        void opcodeCMP_mem(uint16_t  address);
        void opcodeCPX_mem(uint16_t  address);
        void opcodeCPY_mem(uint16_t  address);
        void opcodeDEC_acc();
        void opcodeDEC_mem(uint16_t  address);
        void opcodeDEX_imp();
        void opcodeDEY_imp();
        void opcodeEOR_mem(uint16_t  address);
        void opcodeINC_acc();
        void opcodeINC_mem(uint16_t  address);
        void opcodeINX_imp();
        void opcodeINY_imp();
        void opcodeJMP_mem(uint16_t  address);
        void opcodeJSR_mem(uint16_t  address);
        void opcodeLDA_mem(uint16_t  address);
        void opcodeLDX_mem(uint16_t  address);
        void opcodeLDY_mem(uint16_t  address);
        void opcodeLSR_acc();
        void opcodeLSR_mem(uint16_t  address);
        void opcodeNOP_imp(uint8_t  paddingBytes);
        void opcodeORA_mem(uint16_t  address);
        void opcodePHA_imp();
        void opcodePHP_imp();
        void opcodePHX_imp();
        void opcodePHY_imp();
        void opcodePLA_imp();
        void opcodePLP_imp();
        void opcodePLX_imp();
        void opcodePLY_imp();
        void opcodeRMB_mem(uint16_t  address, uint8_t  bitNumber);
        void opcodeROL_acc();
        void opcodeROL_mem(uint16_t  address);
        void opcodeROR_acc();
        void opcodeROR_mem(uint16_t  address);
        void opcodeRTI_imp();
        void opcodeRTS_imp();
        void opcodeSBC_mem(uint16_t  address);
        void opcodeSEC_imp();
        void opcodeSED_imp();
        void opcodeSEI_imp();
        void opcodeSMB_mem(uint16_t  address, uint8_t  bitNumber);
        void opcodeSTA_mem(uint16_t  address);
        void opcodeSTP_imp();
        void opcodeSTX_mem(uint16_t  address);
        void opcodeSTY_mem(uint16_t  address);
        void opcodeSTZ_mem(uint16_t  address);
        void opcodeTAX_imp();
        void opcodeTAY_imp();
        void opcodeTRB_mem(uint16_t  address);
        void opcodeTSB_mem(uint16_t  address);
        void opcodeTSX_imp();
        void opcodeTXA_imp();
        void opcodeTXS_imp();
        void opcodeTYA_imp();
        void opcodeWAI_imp();
        
        //Opcode WDM 0x42 is unused by both 6502, WDC65C02 and 65C816, it was intended for future uses.
        //The EZ65C02 uses this opcode to provide up to 256 High Level Instructions.
        //Instruction is 2 bytes long, the first being 0x42 and the second the opcode of the HLI that will be executed. 
        //How parameters are passed depends on the specific routine called. 
        void opcodeHLI_ext();
};

//Declaration of an instance of the Virtual Processing Unit
extern EZ65C02 VPU;

#endif