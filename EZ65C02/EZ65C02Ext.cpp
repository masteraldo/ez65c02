/*
   _____ _______  ____   ____ ___ ____
  | ____|__  / /_| ___| / ___/ _ \___ \
  |  _|   / / '_ \___ \| |  | | | |__) |
  | |___ / /| (_) |__) | |__| |_| / __/
  |_____/____\___/____/ \____\___/_____| P R O J E C T

A WDC65C02 emulator designed for simplicity, portability and extensivity.
It aims to be an opcode interpreter (not cycle accurate) for MCUs and it's the emulator core of the ZerOS project.
WDM opcode is used to provide additional high level routines.

(c) 2021 Eraldo Zanon - EZ65C02 Project
This code is licensed under MIT license (see LICENSE.txt for details)
*/

#include <cstdint>
#include "EZ65C02.h"

//A routine to read a byte from an external memory address, be it through SPI or another bus
uint8_t EZ65C02::readExtMem(uint16_t address)
{
    return 0;
}

//A routine to write a byte to an external memory address, be it through SPI or another bus
void EZ65C02::writeExtMem(uint16_t address, uint8_t data)
{

}

//A routine to read 2 bytes from an external memory address, be it through SPI or another bus
uint16_t EZ65C02::readExtMem16(uint16_t address)
{
    return 0;
}

//A routine to write 2 bytes to an external memory address, be it through SPI or another bus
void EZ65C02::writeExtMem16(uint16_t address, uint16_t data)
{

}

//EZ65C02 extension opcode to execute High Level Instructions
//The byte used is the same as the WDM opcode which was intended specifically for future uses on WDC65C02 and derivatives
//This is a 2 bytes instruction, thi first is the opcode 0x42 and the second specifies the High Level Instruction to execute.
//Every HLI may use a different addressing mode
void EZ65C02::opcodeHLI_ext()
{
    uint8_t hliOpcode = readMem(this->regPC);

	++this->regPC;

	switch (hliOpcode)
	{
		//Insert here High Level Instruction opcodes 

		default:  break;
	}
}